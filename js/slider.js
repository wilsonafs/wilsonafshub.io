$(document).ready(function(){
  
  $(".Modern-Slider").slick({
    autoplay:true,
    autoplaySpeed:4000,
    speed:600,
    slidesToShow:1,
    slidesToScroll:1,
    pauseOnHover:true,
    dots:true,
    pauseOnDotsHover:true,
    cssEase:'linear',
   // fade:true,
    draggable:false
  });
  
})